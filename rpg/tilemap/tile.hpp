#pragma once

#include <array>
#include <cstdint>
#include <functional>

#include "tile_drawer.hpp"

class Tile
{
public:
    Tile(std::array<uint8_t, 8> bitmap, uint8_t attr);

    bool operator==(const Tile& other) const noexcept;
    bool operator!=(const Tile& other) const noexcept;

    Tile_drawer drawer() const;

private:
    std::array<uint8_t, 8> bitmap;
    uint8_t attr;

    friend struct Tile_hash;
};

struct Tile_hash
{
    std::size_t operator()(const Tile& t) const noexcept;
};
