#pragma once

#include <ostream>

namespace Drawer
{
    class Operation
    {
    public:
        virtual ~Operation() = default;

        virtual void print(std::ostream& out) const = 0;

    private:

    };

    std::ostream& operator<<(std::ostream& out, const Operation& op);
}
