#pragma once

#include <memory>
#include <ostream>
#include <vector>

#include "operation.hpp"

class Tile_drawer
{
public:
    Tile_drawer& add(std::unique_ptr<Drawer::Operation> op);

    std::ostream& operator<<(std::ostream& out) const;

private:
    std::vector<std::unique_ptr<Drawer::Operation>> ops;
};
