#include "screen.hpp"

#include <fstream>

Screen::Screen(std::filesystem::path path, bool attributes)
    :tilemap {32, 24}
{
    // load in screen

    std::ifstream file {path, std::ios::binary};
    file.exceptions(std::ifstream::badbit | std::ifstream::failbit | std::ifstream::eofbit);

    std::vector<uint8_t> linear_screen (6912);

    for (int i {0}; i != 6144; ++i)
    {
        auto coords {address_to_coords(16384 + i)};
        linear_screen[32 * coords.first + coords.second] = file.get();
    }

    for (int i {0}; i != 768; ++i)
    {
        linear_screen[6144 + i] = file.get();
    }

    // convert to tiles

    for (int y {0}; y != 24; ++y)
    {
        for (int x {0}; x != 32; ++x)
        {
            std::array<uint8_t, 8> bitmap {};

            for (int z {0}; z != 8; ++z)
            {
                bitmap[z] = linear_screen[256 * y + 32 * z + x];
            }

            uint8_t attr {attributes ? linear_screen[32 * y + x + 6144] : 0x78u};
            tilemap.add(y, x, Tile {bitmap, attr});
        }
    }
}

uint8_t Screen::tiles() const noexcept
{
    return tilemap.tiles();
}

std::pair<int, int> Screen::address_to_coords(int adr)
{
    int y {((adr >> 5) & 0b1100'0000) | ((adr >> 2) & 0b0011'1000) | ((adr >> 8) & 0b0000'0111)};
    int x {adr & 0b0001'1111};

    return {x, y};
}

int Screen::coords_to_address(int y, int x)
{
    int high {0b0100'0000 | ((y >> 3) & 0b0001'1000) | (y & 0b0000'0111)};
    int low {((y << 2) & 0b1110'0000) | x};

    return (high << 8) | low;
}
