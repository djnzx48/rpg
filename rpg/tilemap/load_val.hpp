#pragma once

#include <cstdint>

#include "operation.hpp"

namespace Drawer
{
    class Load_val : public Operation
    {
    public:
        Load_val(uint8_t val);

        void print(std::ostream& out) const override;

    private:
        uint8_t val;
    };
}
