#pragma once

#include "tilemap.hpp"

#include <filesystem>

// temporary hack for vs 2015
namespace std
{
    namespace filesystem = experimental::filesystem::v1;
}

class Screen
{
public:
    Screen(std::filesystem::path path, bool attributes = true);

    uint8_t tiles() const noexcept;

private:

    // coords is in rows, char columns format
    static std::pair<int, int> address_to_coords(int adr);
    static int coords_to_address(int y, int x);

    Tilemap tilemap;

};
