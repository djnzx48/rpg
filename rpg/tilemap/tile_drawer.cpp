#include "tile_drawer.hpp"

Tile_drawer& Tile_drawer::add(std::unique_ptr<Drawer::Operation> op)
{
    ops.push_back(std::move(op));

    return *this;
}

std::ostream& Tile_drawer::operator<<(std::ostream& out) const
{
    for (const auto& op : ops)
    {
        out << op;
    }

    return out;
}
