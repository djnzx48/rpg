#pragma once

#include "operation.hpp"

namespace Drawer
{
    class Inc_scr : public Operation
    {
    public:
        void print(std::ostream& out) const override;

    private:
    };
}
