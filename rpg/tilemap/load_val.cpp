#include "load_val.hpp"

Drawer::Load_val::Load_val(uint8_t val)
    :val {val}
{
}

void Drawer::Load_val::print(std::ostream& out) const
{
    out << "ld (hl), " << val << '\n';
}
