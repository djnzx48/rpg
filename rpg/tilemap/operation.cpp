#include "operation.hpp"

std::ostream& Drawer::operator<<(std::ostream& out, const Drawer::Operation& op)
{
    op.print(out);
    return out;
}
