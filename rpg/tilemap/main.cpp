#include "screen.hpp"
#include "tilemap.hpp"

#include <iostream>

int main()
{
    Screen scr {"flashback_fg.scr", false};

    std::cout << "Finished with " << int {scr.tiles()} << " unique tiles.\n";

    std::cout << "Press a key...\n";
    char c;
    std::cin >> c;
}
