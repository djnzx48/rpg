#pragma once

#include "tile.hpp"

#include <stdexcept>
#include <unordered_map>

class Tilemap
{
public:
    Tilemap(unsigned width, unsigned height);

    Tilemap& add(unsigned y, unsigned x, const Tile& tile);

    uint8_t tiles() const noexcept;

    class Tile_limit_exception : public std::logic_error
    {
    public:
        explicit Tile_limit_exception(const std::string& arg)
            :logic_error{arg} {}
    };

private:

    // TODO: don't forget about INVERTED tiles.
    // TODO: tile OPTIMIZER.

    unsigned width;
    unsigned height;

    std::vector<uint8_t> tilemap;
    std::unordered_map<Tile, uint8_t, Tile_hash> ids;
    unsigned index {0};
};
