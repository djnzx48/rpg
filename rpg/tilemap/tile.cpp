#include "tile.hpp"

#include <memory>

#include "inc_scr.hpp"
#include "load_val.hpp"

Tile::Tile(std::array<uint8_t, 8> bitmap, uint8_t attr)
    :bitmap {bitmap}, attr {attr}
{}

bool Tile::operator==(const Tile& other) const noexcept
{
    return bitmap == other.bitmap && attr == other.attr;
}

bool Tile::operator!=(const Tile& other) const noexcept
{
    return !(*this == other);
}

Tile_drawer Tile::drawer() const
{
    Tile_drawer drawer {};

    // TODO: fix this to use actual logic

    for (uint8_t val : bitmap)
    {
        drawer.add(std::make_unique<Drawer::Load_val>(val));
        drawer.add(std::make_unique<Drawer::Inc_scr>());
    }

    return drawer;
}

std::size_t Tile_hash::operator()(const Tile& t) const noexcept
{
    // 64-bit FNV hash

    constexpr uint64_t prime {1099511628211};
    constexpr uint64_t offset_basis {14695981039346656037};

    uint64_t hash {offset_basis};

    for (uint8_t x : t.bitmap)
    {
        hash = (hash * prime) ^ x;
    }

    hash = (hash * prime) ^ t.attr;

    return hash;
}
