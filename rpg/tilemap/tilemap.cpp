#include "tilemap.hpp"

#include <cassert>

Tilemap::Tilemap(unsigned width, unsigned height)
    :width {width}, height {height}, tilemap(width * height)
{}

Tilemap& Tilemap::add(unsigned y, unsigned x, const Tile& tile)
{
    auto elem {ids.find(tile)};

    if (elem == ids.end())
    {
        if (index == 256)
            throw Tile_limit_exception {"Tried to add more than 256 unique tiles!"};

        auto result {ids.insert({tile, index++})};
        assert(result.second);
        elem = result.first;
    }

    tilemap[width * y + x] = elem->second;

    return *this;
}

uint8_t Tilemap::tiles() const noexcept
{
    assert(index <= 256);
    return index;
}
